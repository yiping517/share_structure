package concept;

public class Demo {

	public static void main(String[] args) {

		Cell c = new Cell();
		Cell cc = c; //指向同一個對象
		c.row = 2;
		cc.row = 5;
		System.out.println(c.row);
		
		int a = 9;
		int b = a;//賦值
		b = 8;
		System.out.println(a);
		
		Cell c1 = new Cell();
		c1.row = 2;
		c1 = null;
		c1.row = 9;
	}

}
